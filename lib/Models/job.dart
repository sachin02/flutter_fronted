class Job {
  int? id;
  String? title;
  String? company;
  String? website;
  String? address;
  int? amount;
  int? intershipStipend;
  int? totalExperience;
  int? experienceMonth;
  String? lastDateToApply;
  int? driveDate;
  String? driveLocation;
  bool? isDone;
  String? image;

  Job(
      {this.id,
      this.title,
      this.company,
      this.website,
      this.address,
      this.amount,
      this.intershipStipend,
      this.totalExperience,
      this.experienceMonth,
      this.lastDateToApply,
      this.driveDate,
      this.driveLocation,
      this.isDone,
      this.image});

  Job.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    company = json['company'];
    website = json['website'];
    address = json['address'];
    amount = json['amount'];
    intershipStipend = json['intershipStipend'];
    totalExperience = json['totalExperience'];
    experienceMonth = json['experienceMonth'];
    lastDateToApply = json['LastDateToApply'];
    driveDate = json['DriveDate'];
    driveLocation = json['DriveLocation'];
    isDone = json['isDone'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['company'] = this.company;
    data['website'] = this.website;
    data['address'] = this.address;
    data['amount'] = this.amount;
    data['intershipStipend'] = this.intershipStipend;
    data['totalExperience'] = this.totalExperience;
    data['experienceMonth'] = this.experienceMonth;
    data['LastDateToApply'] = this.lastDateToApply;
    data['DriveDate'] = this.driveDate;
    data['DriveLocation'] = this.driveLocation;
    data['isDone'] = this.isDone;
    data['image'] = this.amount;
    return data;
  }
}
