import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:job_cards/Constants/api.dart';
import 'package:dio/dio.dart';
import 'Models/job.dart';
import 'content.dart';
import 'dart:convert';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Job> myJob = [];
  bool isLoading = true;
  void fetchdata() async {
    try {
      Response response = await Dio().get('http://127.0.0.1:8000/jobs');
      print(response.data.toString());
      // print(response.data.runtimeType);
      response.data.forEach((jobs) => myJob.add(Job.fromJson(jobs)));
      setState(() {
        isLoading = false;
      });
    } catch (e) {
      print("Error is ${e.toString()}");
    }
  }

  void delete_jobs(String id) async {
    try {
      Response response = await Dio().delete('http://127.0.0.1:8000/jobs');
      fetchdata();
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    fetchdata();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Jobs Page"),
        backgroundColor: Colors.deepOrange,
        centerTitle: true,
      ),
      body: Center(
        child: Container(
          color: Colors.white.withRed(10),
          margin: EdgeInsets.all(15),
          height: 700,
          width: 900,
          child: ListView.builder(
              itemCount: myJob.length,
              itemBuilder: (BuildContext context, int index) {
                return Content(
                  job: myJob[index],
                  onPress: () => delete_jobs(myJob.toString()),
                );
              }),
        ),
      ),
    );
  }
}
